<?php

$EmojiUtf8Byte = '\xF0\x9F\x98\x81';

$pattern = '@\\\x([0-9a-fA-F]{2})@x';
$emoji = preg_replace_callback(
  $pattern,
  function ($captures) {
    return chr(hexdec($captures[1]));
  },
  $utf8Byte
);

$telegramResponseText = "Hey user " . $emoji;

var_dump($emoji);
