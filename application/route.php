<?php
return [
    '__pattern__' => [
        'name' => '\w+',
    ],
    '[hello]'     => [
        ':id'   => ['index/hello', ['method' => 'get'], ['id' => '\d+']],
        ':name' => ['index/hello', ['method' => 'post']],
    ],
    
        //Index
    'index'=>'index/Index/index',//
    
    'setting_pwd'=>'admin/Admin/setting_pwd',
    
    'admin'=>'admin/Admin/admin',
    'admin_update'=>'admin/Admin/admin_update',
    'message'=>'admin/Admin/message',
    'setting_bot'=>'admin/Admin/setting_bot',//机器人api配置
    'huifu'=>'admin/Admin/huifu',
    'huifu_zn'=>'admin/Admin/huifu_zn',
    'huifu_bq'=>'admin/Admin/huifu_bq',
    'huifu_edit'=>'admin/Admin/huifu_edit',
    
    'botlist'=>'admin/Admin/botlist',
    'delete_bot'=>'admin/Admin/delete_bot',
    
    'msg_del'=>'admin/Admin/msg_del',
    
    'btn'=>'admin/Admin/btn',
    'btn_zn'=>'admin/Admin/btn_zn',
    'add_btn'=>'admin/Admin/add_btn',
    'delete_btn'=>'admin/Admin/delete_btn',
    
    'group'=>'admin/Admin/group',
    'delete_group'=>'admin/Admin/delete_group',
    'corn_add'=>'admin/Admin/corn_add',
    'corn_add2'=>'admin/Admin/corn_add2',
    'corn'=>'admin/Admin/corn',
    'corn_update'=>'admin/Admin/corn_update',
    'delete_corn'=>'admin/Admin/delete_corn',
    
    'login'=>'admin/login/login',
    'admin/Login/outlogin'=>'admin/login/outlogin',
    'api'=>'admin/Api/index',//telegram回调接口
    'api/corn'=>'admin/Api/corn',//推送接口
    'api1'=>'admin/Api/ceshi',//api测试
    'shangpin'=>'admin/admin/shangpin',
    'shangpin_gid'=>'admin/Admin/shangpin_gid',
    
    'add_token'=>'admin/Admin/add_token',//保存密钥
    'add_api'=>'admin/Admin/add_api',//保存api
    'delete_api'=>'admin/Admin/delete_api',//删除api
    'dulogin'=>'admin/Login/dulogin',
    'add_shangpingid'=>'admin/Admin/add_shangpingid',//保存商品分类
    'delete_shangpingid'=>'admin/Admin/delete_shangpingid',//删除商品分类
    'add_shangpin'=>'admin/Admin/add_shangpin',//保存商品信息
    'update_img'=>'admin/Admin/update_img',//图片封面
    'update_shangpinimg'=>'admin/Admin/update_shangpinimg',//商品图片存
    
    
    'index'=>'admin/Index/index',

];
