# Telegram bot admin

#### 介绍
telegram 机器人自动化管理后台，支持多机器人、关键词回复、设置按钮、支持个人、群、频道等的消息处理，另外支持消息定时推送。
## 演示  [Telegram bot admin](https://tgbott.com/login) 

#### 软件架构
Thinkphp5.0

#### 安装教程

1.  Thinkphp安装方式设置访问目录和伪静态
2.  配置数据库文件(编码为utf8mb4_general_ci)
3.  后台地址：host/admin
4.  账号:admin 密码:123456

#### 使用说明

1.  demo [tgbott.com](https://tgbott.com/login) 
2.  按钮的设置和消息推送的设置都是基于回复模板
3.  按钮可设置多个，默认为4个一行
4.  在按钮或者消息中显示表情 使用表情的UTF-8编码 并将\x替换为% 例如 \xF0\x9F\x98\x81 写为 %F0%9F%98%81 即可显示对应表情
5.  emoji [参考网址](https://apps.timwhitlock.info/emoji/tables/unicode)
6.  在消息中添加图片 使用markdown方式 
    ```
    ![百度](http://static.runoob.com/images/runoob-logo.png)
    ```

7.  特殊字符无法输出 使用斜杠转义 例如要显示_ 输入时应当是 \_   
8.  联系方式 
    
    邮箱 2597403584@qq.com

    [Telegram bot admin QQ交流群](https://qm.qq.com/cgi-bin/qm/qr?k=djYR22qpwAa1uBlfStLLZFyTN8pyrERy&authKey=7bjHNMdOJDxwQwCl07uTPMsdIxujsLuj2OHZuYDL8xkTZ6vlJr6bymyOytYoqMYL&noverify=0&group_code=970205920)
    
    [Telegram bot admin 电报交流群](https://t.me/tgbot_clouds)

    

#### 参与贡献

[Telegram-bot 机器人](https://gitee.com/pmhw/telegram-bot?_from=gitee_search#%E5%8F%82%E4%B8%8E%E8%B4%A1%E7%8C%AE)
