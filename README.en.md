# Telegram bot admin

#### Description
telegram 机器人自动化管理后台，支持多机器人、关键词回复、设置按钮、支持个人、群、频道等的消息处理，另外支持消息定时推送。

#### Software Architecture
Thinkphp5.0

#### Installation

1.  Thinkphp安装方式设置访问目录和伪静态
2.  配置数据库文件(编码为utf8mb4_general_ci)
3.  后台地址：host/admin
4.  账号:admin 密码:123456

#### Instructions

1.  demo [tgbott.com](https://tgbott.com/login) 
2.  按钮的设置和消息推送的设置都是基于回复模板
3.  按钮可设置多个，默认为4个一行
4.  在按钮或者消息中显示表情 使用表情的UTF-8编码 并将\x替换为% 例如 \xF0\x9F\x98\x81 写为 %F0%9F%98%81 即可显示对应表情
5.  emoji [参考网址](https://apps.timwhitlock.info/emoji/tables/unicode)
6.  在消息中添加图片 使用markdown方式 
    '''
    ![百度](http://static.runoob.com/images/runoob-logo.png)
    '''
7.  特殊字符无法输出 使用斜杠转义 例如要显示_ 输入时应当是 \_   
8.  
#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
